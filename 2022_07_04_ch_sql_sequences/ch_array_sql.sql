WITH
  source AS (
  SELECT
    toUInt64(JSONExtractString(json, 'id'))                         AS id,
    parseDateTimeBestEffort(JSONExtractString(json, 'captured_at')) AS captured_at,
    JSONExtract(json, 'viewer_count', 'UInt32')                     AS viewer_count,
    lagInFrame(viewer_count) OVER (
      PARTITION BY id ORDER BY captured_at ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING
    )                                                               AS prev_count
  FROM _local.table
)
, diffs AS (
  SELECT 
    id,
    toUnixTimestamp(captured_at)             AS diff_unix
  FROM source
  WHERE viewer_count != prev_count
  ORDER BY id, captured_at -- this is for groupArray to maintain order
)
, id_arr AS (
  SELECT
    id,
    groupArray(diff_unix)                      AS arr_diffs_unix,
    arrayPopFront(arr_diffs_unix)              AS full_seq_arr, -- remove first change because it is incomplete
    arrayDifference(full_seq_arr)              AS seconds_diffs,
    arrayPopFront(seconds_diffs)               AS clean_seconds_diffs, -- remove first element because it is introduced by function arrayDifference
    arrayMap((x) -> x/60, clean_seconds_diffs) AS arr_minutes,
    arrayMap((x) -> round(x), arr_minutes)     AS arr_minutes_rounded
  FROM diffs
  GROUP BY id
)
, all_diffs_arr AS (
  SELECT
    groupArray(arr_minutes_rounded) AS arr_diffs, -- array of arrays
    arrayFlatten(arr_diffs)         AS change_arr
  FROM id_arr
)
SELECT
  length(change_arr)                 AS num_changes,
  arrayReduce('median', change_arr)  AS median_minutes,
  arrayMin(change_arr)               AS min_minutes,
  arrayMax(change_arr)               AS max_minutes,
  arrayAvg(change_arr)               AS avg_minutes,
  arrayReduce('topK(5)', change_arr) AS most_freq_durations
FROM all_diffs_arr