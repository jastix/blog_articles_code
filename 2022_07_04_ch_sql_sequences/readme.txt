How to run these SQL queries on sample data:

```
-- from the directory with queries and sample data
docker run -v $(pwd):/q -v $(pwd):/data clickhouse/clickhouse-server:22.5 clickhouse-local --input-format "JSONAsString" --output-format "Pretty" --queries-file "/q/ch_array_sql.sql" --file "/data/sample_data.json.gz"
```