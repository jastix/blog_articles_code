WITH
  source AS (
  SELECT 
    toUInt64(JSONExtractString(json, 'id'))                         AS id,
    parseDateTimeBestEffort(JSONExtractString(json, 'captured_at')) AS captured_at,
    JSONExtract(json, 'viewer_count', 'UInt32')                     AS viewer_count,
    lagInFrame(viewer_count) OVER (
      PARTITION BY id ORDER BY captured_at ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING
    )                                                               AS prev_count,
    lagInFrame(captured_at)  OVER (
      PARTITION BY id ORDER BY captured_at ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING
    )                                                               AS prev_captured_at
  FROM _local.table
)
, streams_with_changes AS (
  SELECT 
    id,
    captured_at
  FROM source
  WHERE toYear(prev_captured_at) != 1970 -- filter first row which is marked as change
    AND viewer_count != prev_count
)
, only_changed AS (
  SELECT
    captured_at,
    lagInFrame(captured_at) OVER (PARTITION BY id ORDER BY captured_at ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS prev_change_ts
  FROM streams_with_changes
)
, diffs AS (
  SELECT
    dateDiff('minute',   prev_change_ts, captured_at) AS minutes_diff
  from only_changed
  WHERE toYear(prev_change_ts) != 1970 -- if we use LAG then we should start with second row
)
SELECT 
  count(*)             AS num_of_changes,
  median(minutes_diff) AS median_minutes,
  min(minutes_diff)    AS min_minutes,
  max(minutes_diff)    AS max_minutes,
  avg(minutes_diff)    AS avg_minutes
FROM diffs